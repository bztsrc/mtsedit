# Generated by MTSEdit for Minetestmapper colors.txt
# using node palette Mineclone2

mcl_core:acaciatree_bark  72  68  61
mcl_stairs:slab_acaciatree_bark  80  75  67
mcl_stairs:stair_acaciatree_bark  72  67  61
mesecons_button:button_acaciawood_off 137  74  41
mcl_fences:acacia_fence 132  70  39
mcl_fences:acacia_fence_gate 134  72  40
mcl_core:acacialeaves  39  98  13
mcl_core:acaciatree  95  73  59
mcl_core:acaciawood 102  39  23
mesecons_pressureplates:pressure_plate_acaciawood_on 155  83  46
mcl_flowerpots:flower_pot_acaciasapling  96  78  29
mcl_core:acaciasapling 119 121  24
mcl_stairs:slab_acaciawood 108  76  57
mcl_stairs:stair_acaciawood  93  73  59
mcl_doors:acacia_trapdoor 126  69  40
mcl_minecarts:activator_rail 123 109  87
mcl_flowerpots:flower_pot_allium 105  70  73
mcl_flowers:allium 168 139 196
mcl_core:andesite 136 135 127
mcl_stairs:slab_andesite 142 140 132
mcl_stairs:stair_andesite 137 136 128
mcl_walls:andesite 108 108 109
mcl_anvils:anvil  57  57  57
mcl_flowerpots:flower_pot_azure_bluet 108  90  61
mcl_flowers:azure_bluet 174 207 135
mcl_core:barrier 213   0   0
mcl_beds:red  99  43  36
mcl_core:bedrock  67  65  64
mcl_core:bell 125 117  77
mcl_core:birchtree_bark 142 142 139
mcl_stairs:slab_birchtree_bark 155 155 151
mcl_stairs:stair_birchtree_bark 140 140 137
mesecons_button:button_birchwood_off 157 143  99
mcl_doors:birch_door 165 154 120
mcl_fences:birch_fence 152 138  96
mcl_fences:birch_fence_gate 154 140  97
mcl_core:birchleaves  63  81  41
mcl_core:birchtree 164 160 144
mcl_core:birchwood 151 138  95
mesecons_pressureplates:pressure_plate_birchwood_off 178 162 112
mcl_flowerpots:flower_pot_birchsapling  99  94  54
mcl_core:birchsapling 121 157  74
mcl_stairs:slab_birchwood 169 162 139
mcl_stairs:stair_birchwood 164 159 145
mcl_doors:birch_trapdoor 165 154 121
mcl_beds:bed_black_bottom  68  65  62
mcl_wool:black_carpet  19  20  24
mcl_colorblocks:concrete_black   6   8  12
mcl_colorblocks:concrete_powder_black  19  21  24
mcl_stairs:slab_concrete_black   7   8  13
mcl_stairs:stair_concrete_black   6   9  12
mcl_colorblocks:glazed_terracotta_black  54  24  26
mcl_chests:black_shulker_box  19  20  23
mcl_core:glass_black  17  17  17
xpanes:pane_black_flat  19  19  19
mcl_colorblocks:hardened_clay_black  29  17  12
mcl_wool:black  16  16  20
mcl_beds:bed_blue_bottom  85  94 138
mcl_wool:blue_carpet  50  54 149
mcl_colorblocks:concrete_blue  34  36 112
mcl_colorblocks:concrete_powder_blue  55  58 131
mcl_stairs:slab_concrete_blue  37  39 120
mcl_stairs:stair_concrete_blue  34  37 112
mcl_colorblocks:glazed_terracotta_blue  38  51 112
mcl_flowerpots:flower_pot_blue_orchid  76  79  73
mcl_flowers:blue_orchid  47 164 173
mcl_chests:blue_shulker_box  31  32  99
mcl_core:glass_blue  40  60 142
xpanes:pane_blue_flat  39  59 138
mcl_colorblocks:hardened_clay_blue  58  46  71
mcl_wool:blue  41  45 124
mcl_core:bone_block 174 171 155
mcl_books:bookshelf  78  62  33
mcl_core:brewing_stand  96  85  72
mcl_core:brick_block  81  51  49
mcl_stairs:slab_brick_block  86  53  51
mcl_stairs:stair_brick_block  82  51  49
mcl_walls:brick 119  77  66
mcl_beds:bed_brown_bottom 121  93  69
mcl_wool:brown_carpet 108  67  38
mcl_colorblocks:concrete_brown  75  46  24
mcl_colorblocks:concrete_powder_brown  98  66  42
mcl_stairs:slab_concrete_brown  80  49  26
mcl_stairs:stair_concrete_brown  75  47  24
mcl_colorblocks:glazed_terracotta_brown  96  85  69
mcl_mushrooms:mushroom_brown 155 117  92
mcl_flowerpots:flower_pot_mushroom_brown 101  63  49
mcl_chests:brown_shulker_box  74  46  25
mcl_core:glass_brown  77  58  40
xpanes:pane_brown_flat  78  58  39
mcl_colorblocks:hardened_clay_brown  60  40  27
mcl_wool:brown  90  56  32
mcl_furnaces:furnace_active  58  56  54
mcl_core:cactus  37  78  17
mcl_flowerpots:flower_pot_cactus  78  78  36
mcl_cake:cake 186 155 140
mcl_cauldrons:cauldron  56  55  57
mcl_chests:chest  95  74  44
mcl_nether:quartz_chiseled 184 180 175
mcl_core:redsandstonecarved 144  76  22
mcl_end:chorus_flower 101 140  39
mcl_end:chorus_plant 129 107 129
mcl_core:clay 127 127 127
mcl_core:coalblock  32  32  32
mcl_core:stone_with_coal  58  56  56
mcl_core:coarse_dirt  94  67  46
mcl_core:cobble  60  58  56
mcl_stairs:slab_cobble  63  61  59
mcl_stairs:stair_cobble  60  59  56
mcl_walls:cobble  61  58  57
mcl_core:cobweb 159 162 163
mesecons_commandblock:commandblock_off 133 103  85
mcl_stairs:slab_stonebrickcracked  70  68  67
mcl_stairs:stair_stonebrickcracked  67  65  65
mcl_crafting_table:crafting_table  72  54  34
mcl_heads:creeper  86 138  80
mcl_core:redsandstonesmooth 147  79  25
mcl_core:sandstonesmooth 138 135  99
mcl_beds:bed_cyan_bottom  62 124 118
mcl_wool:cyan_carpet  20 130 137
mcl_colorblocks:concrete_cyan  17  93 107
mcl_colorblocks:concrete_powder_cyan  28 117 123
mcl_stairs:slab_concrete_cyan  18  99 113
mcl_stairs:stair_concrete_cyan  17  94 107
mcl_colorblocks:glazed_terracotta_cyan  42  94 101
mcl_chests:cyan_shulker_box  15  83  93
mcl_core:glass_cyan  58  97 119
xpanes:pane_cyan_flat  59  98 118
mcl_colorblocks:hardened_clay_cyan  68  72  72
mcl_wool:cyan  16 108 114
mcl_flowers:dandelion 194 159  17
mcl_flowerpots:flower_pot_dandelion 102  75  39
mcl_core:darktree  49  35  19
mcl_core:darktree_bark  36  28  16
mcl_stairs:slab_darktree_bark  39  31  17
mcl_stairs:stair_darktree_bark  35  27  16
mesecons_button:button_darkwood_off  55  36  16
mcl_fences:dark_oak_fence  52  33  15
mcl_fences:dark_oak_fence_gate  52  34  16
mcl_core:darkleaves  39  99  13
mcl_core:darkwood  52  35  16
mesecons_pressureplates:pressure_plate_darkwood_on  60  39  18
mcl_flowerpots:flower_pot_darksapling  69  65  33
mcl_core:darksapling  59  94  31
mcl_stairs:slab_darkwood  52  36  19
mcl_stairs:stair_darkwood  48  35  19
mcl_doors:dark_oak_trapdoor  60  40  18
mcl_stairs:slab_prismarine_dark  43  77  64
mcl_stairs:stair_prismarine_dark  41  72  60
mesecons_solarpanel:solar_panel_on  89  78  60
mcl_core:deadbush 107  79  40
mcl_flowerpots:flower_pot_deadbush  92  59  37
mcl_end:chorus_flower_dead  92  60  37
mcl_minecarts:detector_rail 120 103  88
mcl_core:diamondblock  78 185 178
mcl_core:stone_with_diamond  81  88  87
mcl_core:diorite 148 148 149
mcl_stairs:slab_diorite 158 158 158
mcl_stairs:stair_diorite 148 149 149
mcl_walls:diorite 149 149 150
mcl_core:dirt  65  43  25
mcl_hoppers:hopper_disabled  61  60  61
mcl_hoppers:hopper_side_disabled  61  61  61
mcl_dispensers:dispenser  93  92  92
mcl_stairs:slab_acaciatree_bark_double  75  71  64
mcl_stairs:slab_acaciawood_double 101  75  58
mcl_stairs:slab_andesite_double 139 137 129
mcl_stairs:slab_birchtree_bark_double 150 150 146
mcl_stairs:slab_birchwood_double 169 163 144
mcl_stairs:slab_concrete_black_double   6  10  12
mcl_stairs:slab_concrete_blue_double  35  37 115
mcl_stairs:slab_brick_block_double  83  51  49
mcl_stairs:slab_concrete_brown_double  78  48  25
mcl_stairs:slab_cobble_double  62  59  57
mcl_stairs:slab_stonebrickcracked_double  68  66  65
mcl_stairs:slab_concrete_cyan_double  17  96 110
mcl_stairs:slab_darktree_bark_double  37  29  16
mcl_stairs:slab_darkwood_double  50  35  19
mcl_stairs:slab_prismarine_dark_double  42  74  62
mcl_stairs:slab_diorite_double 153 153 153
mcl_stairs:slab_end_bricks_double 177 182 132
mcl_stairs:slab_goldblock_double 198 169  53
mcl_stairs:slab_granite_double 121  83  69
mcl_stairs:slab_concrete_green_double  59  73  29
mcl_stairs:slab_concrete_grey_double  44  46  49
mcl_stairs:slab_ironblock_double 105 105 106
mcl_stairs:slab_jungletree_bark_double  63  49  19
mcl_stairs:slab_junglewood_double  93  70  37
mcl_stairs:slab_lapisblock_double  25  55 113
mcl_stairs:slab_concrete_light_blue_double  28 110 160
mcl_stairs:slab_concrete_silver_double 101 101  93
mcl_stairs:slab_concrete_lime_double  76 136  19
mcl_stairs:slab_concrete_magenta_double 136  39 128
mcl_stairs:slab_stonebrickmossy_double  93  98  84
mcl_stairs:slab_mossycobble_double  93  99  84
mcl_stairs:slab_nether_brick_double  35  17  21
mcl_stairs:slab_tree_bark_double  73  58  35
mcl_stairs:slab_wood_double  73  57  38
mcl_stairs:slab_concrete_orange_double 181  78   0
mcl_stairs:slab_concrete_pink_double 172  81 115
mcl_stairs:slab_andesite_smooth_double 108 110 109
mcl_stairs:slab_diorite_smooth_double 158 158 159
mcl_stairs:slab_granite_smooth_double 126  87  73
mcl_stairs:slab_stone_double 158 159 159
mcl_stairs:slab_prismarine_brick_double  82 140 130
mcl_stairs:slab_prismarine_double  81 133 121
mcl_stairs:slab_concrete_purple_double  81  25 126
mcl_stairs:slab_purpur_block_double 138 103 138
mcl_stairs:slab_quartzblock_double  96  54  51
mcl_stairs:slab_concrete_red_double 115  26  26
mcl_stairs:slab_red_nether_brick_double  56   6   7
mcl_stairs:slab_redsandstone_double 149  80  24
mcl_stairs:slab_sandstone_double 142 138 102
mcl_stairs:slab_quartz_smooth_double 191 186 181
mcl_stairs:slab_redsandstonesmooth2_double 148  80  26
mcl_stairs:slab_sandstonesmooth2_double 184 176 141
mcl_stairs:slab_sprucetree_bark_double  35  23   5
mcl_stairs:slab_sprucewood_double  93  69  39
mcl_stairs:slab_stonebrick_double  68  67  65
mcl_stairs:slab_stone_rough_double  62  60  57
mcl_flowers:double_grass  90 131  50
mcl_stairs:slab_concrete_white_double 167 172 173
mcl_stairs:slab_concrete_yellow_double 194 141  17
mcl_end:dragon_egg  10   7  13
mcl_droppers:dropper  93  93  92
mcl_core:emeraldblock  34 161  70
mcl_core:stone_with_emerald  92 107  97
mcl_core:enchanting_table  89  77  70
mcl_chests:ender_chest  26  39  39
mcl_portals:portal_end   6  15  24
mcl_portals:end_portal_frame 106 119  91
mcl_portals:end_portal_frame_eye 106 120  91
mcl_end:end_rod 167 161 151
mcl_end:end_stone 172 175 124
mcl_end:end_bricks 172 176 128
mcl_stairs:slab_end_bricks 183 188 137
mcl_stairs:stair_end_bricks 172 177 128
mcl_walls:endbricks 173 177 130
mcl_fire:eternal_fire 207 129  40
mcl_farming:pumpkin 154  91  19
mcl_farming:soil 108  78  54
mcl_fences:fence 127 102  61
mcl_flowerpots:flower_pot_fern  72  64  42
mcl_flowers:fern  59  90  51
mcl_fire:fire 207 130  40
mcl_flowerpots:flower_pot  93  52  40
mcl_core:lava_flowing 139  56   5
mcl_nether:nether_lava_flowing 139  57   5
mclx_core:river_water_flowing  24 121 205
mcl_core:water_flowing  24 122 205
mcl_core:frosted_ice 113 143 199
mcl_furnaces:furnace  58  57  54
mcl_core:glass 140 140 166
xpanes:pane_flat 156 182 186
mcl_nether:glowstone 135 103  66
mcl_core:goldblock 193 164  49
mcl_stairs:slab_goldblock 204 174  55
mcl_stairs:stair_goldblock 192 163  51
mcl_core:stone_with_gold 113 110  99
mcl_core:granite 117  81  67
mcl_stairs:slab_granite 125  86  72
mcl_stairs:stair_granite 117  82  67
mcl_walls:granite 117  81  68
mcl_flowers:grass  91 133  51
mcl_core:dirt_with_grass  52  54  17
mcl_core:dirt_with_dry_grass 116  89  55
mcl_core:grass_path 116  90  55
mcl_core:gravel 103 100  99
mcl_beds:bed_green_bottom  97 108  56
mcl_wool:green_carpet  80 103  25
mcl_colorblocks:concrete_green  57  71  28
mcl_colorblocks:concrete_powder_green  76  93  35
mcl_stairs:slab_concrete_green  61  76  30
mcl_stairs:stair_concrete_green  57  72  28
mcl_colorblocks:glazed_terracotta_green  93 114  54
mcl_chests:dark_green_shulker_box  56  71  24
mcl_core:glass_green 121 244   0
xpanes:pane_green_flat  79  98  39
mcl_colorblocks:hardened_clay_green  59  65  33
mcl_wool:green  66  86  21
mcl_beds:bed_grey_bottom 102 105 101
mcl_wool:grey_carpet  59  64  67
mcl_colorblocks:concrete_grey  42  45  48
mcl_colorblocks:concrete_powder_grey  60  63  66
mcl_stairs:slab_concrete_grey  45  48  51
mcl_stairs:stair_concrete_grey  42  46  48
mcl_colorblocks:glazed_terracotta_grey  66  72  75
mcl_chests:dark_grey_shulker_box  38  41  43
mcl_core:glass_gray  58  58  59
xpanes:pane_gray_flat  58  58  58
mcl_colorblocks:hardened_clay_grey  45  33  27
mcl_wool:grey  49  54  56
mcl_farming:hay_block 148 127  44
mcl_hoppers:hopper  61  62  61
mcl_mushrooms:brown_mushroom_block_stem_full 118  89  64
mcl_mushrooms:brown_mushroom_block_cap_111111 118  90  64
mcl_mushrooms:brown_mushroom_block_stem 118  91  64
mcl_mushrooms:red_mushroom_block_stem_full 158  37  35
mcl_mushrooms:red_mushroom_block_cap_111111 158  38  35
mcl_mushrooms:red_mushroom_block_stem 158  39  35
mcl_heads:steve  61  42  29
mcl_farming:soil_wet 108  79  54
mcl_core:ice 117 144 174
mcl_stairs:slab_ice 122 151 181
mcl_stairs:stair_ice 118 145 175
mcl_monster_eggs:monster_egg_stonebrickcarved  94  93  94
mcl_monster_eggs:monster_egg_cobble  60  60  56
mcl_monster_eggs:monster_egg_stonebrickcracked  60  61  56
mcl_monster_eggs:monster_egg_stonebrickmossy  91  95  83
mcl_monster_eggs:monster_egg_stone  60  62  56
mcl_monster_eggs:monster_egg_stonebrick  60  63  56
mesecons_solarpanel:solar_panel_inverted_on  76  75  70
xpanes:bar_flat 128 128 128
mcl_core:ironblock 104 103 104
mcl_stairs:slab_ironblock 107 107 107
mcl_stairs:stair_ironblock 103 103 103
mcl_doors:iron_door 155 154 156
mcl_core:stone_with_iron  75  62  57
mcl_doors:iron_trapdoor 162 162 162
mcl_itemframes:item_frame 123  90  63
mcl_farming:pumpkin_face_light 160 102  27
mcl_jukebox:jukebox  72  48  34
mcl_core:jungletree_bark  60  47  18
mcl_stairs:slab_jungletree_bark  66  52  20
mcl_stairs:stair_jungletree_bark  60  46  18
mesecons_button:button_junglewood_off 132  95  67
mcl_fences:jungle_fence 126  90  63
mcl_fences:jungle_fence_gate 128  92  64
mcl_core:jungleleaves  41 101  12
mcl_core:jungletree  86  65  33
mcl_core:junglewood 126  91  63
mesecons_pressureplates:pressure_plate_junglewood_on 148 106  74
mcl_flowerpots:flower_pot_junglesapling  67  59  28
mcl_core:junglesapling  47  84  17
mcl_stairs:slab_junglewood 101  76  42
mcl_stairs:stair_junglewood  85  65  32
mcl_doors:jungle_trapdoor 123  89  62
mcl_core:ladder  99  77  43
mcl_core:lapisblock  24  53 110
mcl_core:stone_with_lapis  78  87 104
mcl_stairs:slab_lapisblock  26  58 118
mcl_stairs:stair_lapisblock  24  54 111
mcl_flowers:double_fern  59  91  51
mcl_core:lava_source 139  58   5
mcl_core:leaves  20  33  17
mesecons_walllever:wall_lever_off  97  95  93
mcl_beds:bed_light_blue_bottom  73 131 158
mcl_wool:light_blue_carpet  55 166 206
mcl_colorblocks:concrete_light_blue  27 107 156
mcl_colorblocks:concrete_powder_light_blue  58 142 168
mcl_stairs:slab_concrete_light_blue  29 114 166
mcl_stairs:stair_concrete_light_blue  27 108 156
mcl_colorblocks:glazed_terracotta_light_blue  76 132 168
mcl_chests:lightblue_shulker_box  36 105 136
mcl_core:glass_light_blue  78 118 168
xpanes:pane_light_blue_flat  79 118 168
mcl_colorblocks:hardened_clay_light_blue  89  85 108
mcl_wool:light_blue  45 137 171
mcl_beds:bed_silver_bottom 124 122 111
mcl_wool:silver_carpet 134 134 127
mcl_colorblocks:concrete_silver  98  98  90
mcl_colorblocks:concrete_powder_silver 122 122 116
mcl_stairs:slab_concrete_silver 104 104  96
mcl_stairs:stair_concrete_silver  98  99  90
mcl_colorblocks:glazed_terracotta_silver 115 133 135
mcl_chests:grey_shulker_box  74  74  69
mcl_core:glass_silver 118 118 119
xpanes:pane_silver_flat 118 118 118
mcl_colorblocks:hardened_clay_silver  98 100  90
mcl_wool:silver 111 111 105
mcl_flowers:lilac_top 145 123 137
mcl_beds:bed_lime_bottom 112 149  57
mcl_wool:lime_carpet 106 175  24
mcl_colorblocks:concrete_lime  73 132  19
mcl_colorblocks:concrete_powder_lime  98 148  32
mcl_stairs:slab_concrete_lime  78 141  20
mcl_stairs:stair_concrete_lime  73 133  19
mcl_colorblocks:glazed_terracotta_lime 131 158  45
mcl_chests:green_shulker_box  63 106  19
mcl_core:glass_lime  97 157  19
xpanes:pane_lime_flat  99 158  19
mcl_colorblocks:hardened_clay_lime  81  92  41
mcl_wool:lime  88 145  20
mcl_core:stone_with_redstone_lit  75  74  49
mcl_beds:bed_magenta_bottom 148  78 134
mcl_wool:magenta_carpet 179  65 170
mcl_colorblocks:concrete_magenta 133  37 125
mcl_colorblocks:concrete_powder_magenta 151  66 145
mcl_stairs:slab_concrete_magenta 141  40 133
mcl_stairs:stair_concrete_magenta 133  38 125
mcl_colorblocks:glazed_terracotta_magenta 166  80 154
mcl_chests:magenta_shulker_box 109  37 103
mcl_core:glass_magenta 138  58 168
xpanes:pane_magenta_flat 138  59 168
mcl_colorblocks:hardened_clay_magenta 118  69  85
mcl_wool:magenta 149  54 141
mcl_nether:magma 113  50  25
mcl_farming:beetroot  72 120  53
mcl_farming:carrot  27 138  12
mcl_cocoas:cocoa 120  73  33
mcl_farming:melontige_unconnect  89 114  23
mcl_nether:nether_wart 115  18  17
mcl_farming:potato  25 173  34
mcl_farming:pumpkintige_unconnect 154  92  19
mcl_farming:wheat 175 157  78
mcl_cocoas:cocoa_2 120  74  33
mcl_farming:melon  89 115  23
mcl_mobspawners:spawner  17  25  31
mcl_core:mossycobble  59  61  46
mcl_walls:mossycobble  85  92  73
mcl_core:stonebrickmossy  91  96  83
mcl_stairs:slab_stonebrickmossy  96 102  86
mcl_stairs:stair_stonebrickmossy  90  95  82
mcl_walls:stonebrickmossy  92  96  85
mcl_stairs:slab_mossycobble  96 103  86
mcl_stairs:stair_mossycobble  90  96  82
mcl_core:mycelium  88  72  65
mcl_nether:nether_brick  34  17  20
mcl_fences:nether_brick_fence  34  18  20
mclx_fences:nether_brick_fence_gate  52  36  16
mcl_stairs:slab_nether_brick  37  18  21
mcl_stairs:stair_nether_brick  34  19  20
mcl_walls:netherbrick  35  17  20
mcl_nether:nether_lava_source 139  59   5
mcl_portals:portal   6  16  24
mcl_nether:quartz_ore  92  51  49
mcl_nether:netherrack  76  30  30
mcl_nether:nether_wart_block  90   3   2
mesecons_noteblock:noteblock  70  46  32
mcl_core:tree  66  51  34
mcl_core:tree_bark  71  56  34
mcl_stairs:slab_tree_bark  77  61  37
mcl_stairs:stair_tree_bark  70  55  34
mesecons_button:button_wood_off 133 107  65
mcl_doors:wooden_door 110  86  50
mcl_fences:fence_gate 130 105  63
mcl_core:ladder  99  78  43
mcl_core:wood  89  68  36
mesecons_pressureplates:pressure_plate_wood_on 148 120  74
mcl_core:sapling  76 109  41
mcl_flowerpots:flower_pot_sapling  77  73  37
mcl_stairs:slab_wood  84  65  44
mcl_stairs:stair_wood  62  48  32
mcl_signs:wall_sign 114  94  56
mcl_observers:observer_off  74  74  74
mcl_core:obsidian  12  13  16
xpanes:obsidian_pane_flat  19  20  19
mcl_stairs:slab_obsidian_glass  22  22  22
mcl_stairs:stair_obsidian_glass  22  23  22
mcl_stairs:slab_obsidian  12  13  15
mcl_stairs:stair_obsidian  12  14  15
mcl_beds:bed_orange_bottom 182 112  52
mcl_wool:orange_carpet 228 112  18
mcl_colorblocks:concrete_orange 176  76   0
mcl_colorblocks:concrete_powder_orange 178 103  25
mcl_stairs:slab_concrete_orange 188  81   0
mcl_stairs:stair_concrete_orange 176  77   0
mcl_colorblocks:glazed_terracotta_orange 124 117  74
mcl_chests:orange_shulker_box 148  72  14
mcl_core:glass_orange 166  97  40
xpanes:pane_orange_flat 168  99  39
mcl_colorblocks:hardened_clay_orange 127  65  29
mcl_flowerpots:flower_pot_tulip_orange  87  72  35
mcl_flowers:tulip_orange 102 144  33
mcl_wool:orange 189  93  15
mcl_flowerpots:flower_pot_oxeye_daisy 114  93  69
mcl_flowers:oxeye_daisy 181 204 153
mcl_core:packed_ice 113 144 199
mcl_flowers:peony 112 116 121
mcl_nether:quartz_pillar 187 183 180
mcl_beds:bed_pink_bottom 175 111 126
mcl_wool:pink_carpet 225 135 165
mcl_colorblocks:concrete_pink 167  79 112
mcl_colorblocks:concrete_powder_pink 180 121 143
mcl_stairs:slab_concrete_pink 178  84 119
mcl_stairs:stair_concrete_pink 167  80 112
mcl_colorblocks:glazed_terracotta_pink 187 124 146
mcl_chests:pink_shulker_box 147  80 103
mcl_core:glass_pink 187  97 127
xpanes:pane_pink_flat 188  99 128
mcl_colorblocks:hardened_clay_pink 127  61  61
mcl_flowerpots:flower_pot_tulip_pink  88  76  47
mcl_flowers:tulip_pink 108 161  87
mcl_wool:pink 186 111 135
mesecons_pistons:piston_normal_off 101  90  75
mcl_core:podzol  87  62  35
mcl_core:andesite_smooth 104 106 106
mcl_stairs:slab_andesite_smooth 112 114 113
mcl_stairs:stair_andesite_smooth 105 107 106
mcl_core:diorite_smooth 152 152 153
mcl_stairs:slab_diorite_smooth 162 162 163
mcl_stairs:stair_diorite_smooth 152 153 153
mcl_core:granite_smooth 121  84  70
mcl_stairs:slab_granite_smooth 130  90  75
mcl_stairs:stair_granite_smooth 121  85  70
mcl_core:stone_smooth 152 154 153
mcl_flowers:poppy 118  48  24
mcl_flowerpots:flower_pot_poppy  98  52  38
mcl_minecarts:golden_rail 135 106  70
mcl_ocean:prismarine  78 129 117
mcl_ocean:prismarine_brick  78 135 125
mcl_stairs:slab_prismarine_brick  84 144 134
mcl_stairs:stair_prismarine_brick  78 136 125
mcl_ocean:prismarine_dark  40  73  60
mcl_stairs:slab_prismarine  83 137 124
mcl_stairs:stair_prismarine  78 130 117
mcl_walls:prismarine  78 131 117
mcl_farming:pumpkin_face 154  93  19
mcl_beds:bed_purple_bottom 116  70 137
mcl_wool:purple_carpet 115  39 163
mcl_colorblocks:concrete_purple  78  24 123
mcl_colorblocks:concrete_powder_purple 103  44 139
mcl_stairs:slab_concrete_purple  84  26 131
mcl_stairs:stair_concrete_purple  78  25 123
mcl_colorblocks:glazed_terracotta_purple  87  38 123
mcl_chests:violet_shulker_box  71  22 109
mcl_core:glass_purple  97  48 138
xpanes:pane_purple_flat  98  48 137
mcl_colorblocks:hardened_clay_purple  93  55  67
mcl_wool:purple  96  33 136
mcl_end:purpur_block 134  99 134
mcl_end:purpur_pillar 135 102 135
mcl_stairs:slab_purpur_block 143 106 143
mcl_stairs:stair_purpur_block 132  98 131
mcl_nether:quartz_block  92  52  49
mcl_stairs:slab_quartzblock  97  53  50
mcl_stairs:stair_quartzblock  91  49  47
mcl_minecarts:rail 123 110  87
mcl_core:realm_barrier 213   1   0
mcl_beds:bed_red_bottom 143  72  64
mcl_wool:red_carpet 152  37  32
mcl_colorblocks:concrete_red 111  25  25
mcl_colorblocks:concrete_powder_red 132  42  40
mcl_stairs:slab_concrete_red 119  27  27
mcl_stairs:stair_concrete_red 111  26  25
mcl_colorblocks:glazed_terracotta_red 145  47  42
mcl_mushrooms:mushroom_red 214  67  63
mcl_flowerpots:flower_pot_mushroom_red 114  52  42
mcl_nether:red_nether_brick  55   6   7
mclx_fences:red_nether_brick_fence_gate  52  37  16
mclx_fences:red_nether_brick_fence  34  20  20
mcl_stairs:slab_red_nether_brick  59   6   8
mcl_stairs:stair_red_nether_brick  55   7   7
mcl_walls:rednetherbrick  55   5   7
mcl_core:redsand 150  81  26
mcl_core:redsandstone 145  77  23
mcl_stairs:slab_redsandstone 154  82  25
mcl_stairs:stair_redsandstone 145  78  23
mcl_walls:redsandstone 148  79  23
mcl_chests:red_shulker_box  97  21  21
mcl_core:glass_red 123  41  41
xpanes:pane_red_flat 120  40  40
mesecons:wire_00000000_off  81   0   0
mesecons_torch:redstoneblock 154 154   0
mcl_comparators:comparator_off_comp 134 125 121
mcl_comparators:comparator_on_comp 150 123 118
mesecons_lightstone:lightstone_off 149 150  97
mcl_core:stone_with_redstone  75  75  49
mesecons_delayer:delayer_off 130 122 120
mesecons_delayer:delayer_on 141 118 113
mesecons_torch:mesecon_torch_on 163  71  38
mesecons_torch:mesecon_torch_off 106  71  49
mcl_colorblocks:hardened_clay_red 112  48  37
mcl_flowerpots:flower_pot_tulip_red  86  72  36
mcl_flowers:tulip_red 118  49  24
mcl_wool:red 126  31  27
mcl_sponges:sponge_wet_river_water 154 151  58
mclx_core:river_water_source  24 123 205
mcl_flowers:rose_bush 118  50  24
mcl_core:sand 151 146 111
mcl_core:sandstone 139 136 100
mcl_core:sandstonecarved 137 106  62
mcl_stairs:slab_sandstone 145 142 105
mcl_stairs:stair_sandstone 140 137 101
mcl_walls:sandstone 171 161 125
mcl_ocean:sea_lantern 136 158 150
mcl_hoppers:hopper_side  61  63  61
mcl_heads:skeleton 137 137 137
mcl_core:slimeblock  88 151  72
mcl_nether:quartz_smooth 186 181 177
mcl_stairs:slab_quartz_smooth 198 192 187
mcl_stairs:stair_quartz_smooth 186 181 176
mcl_core:redsandstonesmooth2 145  78  25
mcl_stairs:slab_redsandstonesmooth2 153  83  27
mcl_stairs:stair_redsandstonesmooth2 144  78  25
mcl_core:sandstonesmooth2 179 171 137
mcl_stairs:slab_sandstonesmooth2 190 182 146
mcl_stairs:stair_sandstonesmooth2 179 172 137
mcl_core:snowblock 158 158 167
mcl_core:snow_layer 164 165 174
mcl_nether:soul_sand  64  49  40
mcl_sponges:sponge 154 152  58
mcl_core:sprucetree_bark  35  22   5
mcl_stairs:slab_sprucetree_bark  36  23   6
mcl_stairs:stair_sprucetree_bark  35  24   5
mesecons_button:button_sprucewood_off  94  69  40
mcl_fences:spruce_fence  90  66  38
mcl_fences:spruce_fence_gate  91  67  38
mcl_core:spruceleaves  45  71  45
mcl_core:sprucetree  80  68  56
mcl_core:sprucewood  90  67  38
mesecons_pressureplates:pressure_plate_sprucewood_on 105  77  44
mcl_flowerpots:flower_pot_sprucesapling  66  51  36
mcl_core:sprucesapling  45  61  37
mcl_stairs:slab_sprucewood  97  71  41
mcl_stairs:stair_sprucewood  91  68  38
mcl_doors:spruce_trapdoor  93  70  42
mesecons_pistons:piston_sticky_off  92  96  76
mcl_core:stone  60  64  56
mcl_core:stonebrick  67  66  64
mcl_core:stonebrickcarved 134 105  64
mcl_core:stonebrickcracked  67  67  64
mcl_stairs:slab_stonebrick  70  69  67
mcl_stairs:stair_stonebrick  67  66  65
mcl_walls:stonebrick  61  59  57
mesecons_button:button_stone_off 102 102 102
mesecons_pressureplates:pressure_plate_stone_off 117 117 117
mcl_stairs:slab_stone  63  62  59
mcl_stairs:stair_stone_rough  61  60  57
mcl_core:reeds  72 142  42
mcl_flowers:sunflower 203 157  61
mcl_flowers:tallgrass  87 128  48
mcl_colorblocks:hardened_clay 121  75  54
mcl_tnt:tnt 138  34  34
mcl_torches:torch_wall 121  96  59
mcl_doors:trapdoor  99  79  45
mcl_chests:trapped_chest  95  70  31
mcl_core:vine  32  81  10
mcl_core:void  69  58  71
mcl_flowers:waterlily  16  67  25
mcl_sponges:sponge_wet 135 142  55
mcl_core:water_source  24 124 205
mcl_beds:bed_white_bottom 179 177 172
mcl_wool:white_carpet 221 223 224
mcl_colorblocks:concrete_white 162 167 168
mcl_colorblocks:concrete_powder_white 178 179 179
mcl_stairs:slab_concrete_white 173 178 179
mcl_stairs:stair_concrete_white 162 168 168
xpanes:pane_white 156 183 186
mcl_colorblocks:glazed_terracotta_white 149 168 163
mcl_chests:white_shulker_box 136 140 141
mcl_core:glass_white 196 196 198
xpanes:pane_white_flat 211 211 211
mcl_colorblocks:hardened_clay_white 165 140 127
mcl_flowerpots:flower_pot_tulip_white  88  80  46
mcl_flowers:tulip_white  98 168  74
mcl_heads:wither_skeleton  33  34  34
mcl_wool:white 154 154 154
mcl_beds:bed_yellow_bottom 189 154  60
mcl_wool:yellow_carpet 235 187  37
mcl_colorblocks:concrete_yellow 189 137  17
mcl_colorblocks:concrete_powder_yellow 183 157  43
mcl_stairs:slab_concrete_yellow 201 146  18
mcl_stairs:stair_concrete_yellow 189 138  17
mcl_colorblocks:glazed_terracotta_yellow 187 153  71
mcl_chests:yellow_shulker_box 161 124  27
mcl_core:glass_yellow 175 175  40
xpanes:pane_yellow_flat 177 177  39
mcl_colorblocks:hardened_clay_yellow 146 104  27
mcl_wool:yellow 195 155  31
mcl_heads:zombie  54  85  41
mcl_beds:bed_black_top  68  65  62
mcl_beds:bed_blue_top  85  94 138
mcl_beds:bed_brown_top 121  93  69
mcl_beds:bed_cyan_top  62 124 118
mcl_beds:bed_green_top  97 108  56
mcl_beds:bed_grey_top 102 105 101
mcl_beds:bed_light_blue_top  73 131 158
mcl_beds:bed_silver_top 124 122 111
mcl_beds:bed_lime_top 112 149  57
mcl_beds:bed_magenta_top 148  78 134
mcl_beds:bed_orange_top 182 112  52
mcl_beds:bed_pink_top 175 111 126
mcl_beds:bed_purple_top 116  70 137
mcl_beds:bed_red_top 143  72  64
mcl_beds:bed_white_top 179 177 172
mcl_beds:bed_yellow_top 189 154  60
